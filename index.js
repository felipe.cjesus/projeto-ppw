var formulario = document.querySelector("#formulario")
var botaoPesquisar = document.querySelector("#botao_pesquisar")
var lista = document.querySelector("#impressao_codigo")
var texto = document.querySelector("#mostrar_codigo")
var pesquisa_usuario = document.querySelector("#input_consultar")
var confirmado = document.querySelector("#confirmado")
var resultado = document.querySelector("#resultado")
var url = 'https://exercicio-ppw.herokuapp.com/api/111647'


//função para cadastrar os dados do formulario
function CadastraCodigo(evento){
    evento.preventDefault()

    var cadastro_formulario = {
        codigo: formulario.querySelector("#campo_texto").value,
        linguagem: formulario.querySelector("#linguagem").value,
        titulo: formulario.querySelector("#titulo").value,
        usuario: formulario.querySelector("#usuario").value
    }

    var opcoes = {
        method: 'POST',
        body:JSON.stringify(cadastro_formulario),
        headers: {
            'content-type': 'application/json'
        }
    }
    var requisicao = fetch(url, opcoes)
    var dados = requisicao.then(function(resposta){
        console.log(resposta.status)

        dados.then(function(dados){
            console.log(dados)
        })

        if(resposta.status==200){
            confirmado.textContent = "Código compartilhado com sucesso!"
            confirmado.classList.add("mostrarConfirmado")
        } else {
            confirmado.textContent = "Ocorreu um problema impossível compartilhar!"
            confirmado.classList.add("mostrarConfirmado")
        }
        setTimeout(function(){
            confirmado.classList.remove("mostrarConfirmado")
        } ,5000)
        return resposta.json()
    })
    //Limpa inputs do formulario após enviar dados para o servidor
    formulario.querySelector("#campo_texto").value = ""
    formulario.querySelector("#linguagem").value = ""
    formulario.querySelector("#titulo").value = ""
    formulario.querySelector("#usuario").value = ""
}

//função que retorna os dados pequisando pelo usuario e chama a função retornoPesquisa
function listaCodigos(){

    texto.textContent = ""
    lista.textContent = ""
    var requisicao = fetch(url)
    var resposta = requisicao.then(function(resposta){
        console.log(resposta.status)
        return resposta.json()
    })
    resposta.then(function(dados){
        console.log(dados)
        retornoPesquisa(dados)
    })
}

//função onde lista os dados pesquisados
function retornoPesquisa(dados){

    for(var i=0;i<dados.length;i++){
        
        if(dados[i].usuario == pesquisa_usuario.value){
            var pLinguagem = document.createElement('p')
            var pTitulo = document.createElement('p')
            var pUsuario = document.createElement('p')
            var code = document.createElement('code')
            pLinguagem.textContent = "Tipo de Linguagem: " + dados[i].linguagem
            pTitulo.textContent = "Título do código: " + dados[i].titulo
            pUsuario.textContent = "Usuario: " + dados[i].usuario
            code.textContent = "Code: " + dados[i].codigo
            lista.appendChild(pLinguagem)
            lista.appendChild(pTitulo)
            lista.appendChild(pUsuario)
            texto.appendChild(code)
            resultado.textContent = "Pesquisa realizada com Sucesso!"
            resultado.classList.add("mostrarResultado")
            break
        }
        if(dados[i].usuario != pesquisa_usuario.value){
            resultado.textContent = "Usuario não encontrado!"
            resultado.classList.add("mostrarResultado")
        }        
    }
    setTimeout(function(){
        resultado.classList.remove("mostrarResultado")
    } ,5000)
}
formulario.addEventListener('submit', CadastraCodigo)
botaoPesquisar.addEventListener('click', listaCodigos)